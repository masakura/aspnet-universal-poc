﻿using System;
using System.Data.SQLite;
using System.Web.UI;
using Koda.Application.Hosting;
using Koda.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Koda.Web
{
    public partial class _Default : Page
    {
        private IServiceScope _scope;
        private IServiceProvider _services;

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            _scope = ApplicationHost.CreateScope();
            _services = _scope.ServiceProvider;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            _services = null;
            _scope?.Dispose();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void LoginOld_OnClick(object sender, EventArgs e)
        {
            using (var connection = new SQLiteConnection("DataSource=|DataDirectory|app.db"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "select Password from Customers where CustomerId = @CustomerId";
                    var parameter = command.CreateParameter();
                    parameter.ParameterName = "CustomerId";
                    parameter.Value = CustomerId.Text;
                    command.Parameters.Add(parameter);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            if (reader.GetString(0) == Password.Text)
                            {
                                ErrorText.Text = "ログインしました";
                                return;
                            }

                        ErrorText.Text = "Customer Id またはパスワードが異なります";
                    }
                }
            }
        }

        protected void LoginNew_OnClick(object sender, EventArgs e)
        {
            var loginService = _services.GetRequiredService<ILoginService>();
            if (loginService.Login(CustomerId.Text, Password.Text))
            {
                ErrorText.Text = "ログインしました";
            }
            else
            {
                ErrorText.Text = "Customer Id またはパスワードが異なります";
            }
        }
    }
}