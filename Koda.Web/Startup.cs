﻿using Koda.Application.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Koda.Web
{
    internal sealed class Startup : IStartup
    {
        public void ConfigureService(IServiceCollection services)
        {
            services.AddDefaultDbConnection();
            services.AddApplicationCore();
            services.AddInfrastructure();
        }
    }
}