﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using Koda.Application.Hosting;
using Koda.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Koda.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // アプリケーションのスタートアップで実行するコードです
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ApplicationHost.Initialize<Startup>();
            using (var scope = ApplicationHost.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<IDataInitializer>().Initialize();
            }
        }
    }
}