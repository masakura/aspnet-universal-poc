﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Koda.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <section class="row">
        <asp:Label runat="server" ID="ErrorText"></asp:Label>
    </section>
    <section class="row">
        <div class="col-sm-3">
            Customer Id:
        </div>
        <div class="col-sm-9">
            <asp:TextBox runat="server" ID="CustomerId"></asp:TextBox>
        </div>
    </section>
    <section class="row">
        <div class="col-sm-3">
            Password:
        </div>
        <div class="col-sm-9">
            <asp:TextBox runat="server" ID="Password"></asp:TextBox>
        </div>
    </section>
    <section class="row">
        <asp:Button runat="server" ID="LoginOld" Text="Login (旧式)" OnClick="LoginOld_OnClick"/>
        <asp:Button runat="server" ID="LoginNew" Text="Login (新式)" OnClick="LoginNew_OnClick"/>
    </section>
</asp:Content>
