﻿using Microsoft.Extensions.DependencyInjection;

namespace Koda.Application.Hosting
{
    public interface IStartup
    {
        void ConfigureService(IServiceCollection services);
    }
}