﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Koda.Application.Hosting
{
    public sealed class ApplicationHost
    {
        private static IServiceProvider _services;

        public static void Initialize<T>() where T:IStartup,new()
        {
            var services = new ServiceCollection();
            new T().ConfigureService(services);

            _services = services.BuildServiceProvider();
        }

        public static IServiceScope CreateScope()
        {
            return _services.CreateScope();
        }
    }
}