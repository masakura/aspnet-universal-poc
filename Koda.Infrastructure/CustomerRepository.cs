﻿using System.Data;
using Dapper;
using Koda.Core;

namespace Koda.Infrastructure
{
    internal sealed class CustomerRepository : ICustomerRepository
    {
        private readonly IDbConnection _connection;

        public CustomerRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public Customer Find(string customerId)
        {
            var sql = "select * from Customers where CustomerId = @CustomerId";
            return _connection.QueryFirstOrDefault<Customer>(sql, new {CustomerId = customerId});
        }
    }
}