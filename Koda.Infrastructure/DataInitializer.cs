﻿using System.Data;
using Dapper;
using Dapper.Contrib.Extensions;
using Koda.Core;

namespace Koda.Infrastructure
{
    internal sealed class DataInitializer : IDataInitializer
    {
        private readonly IDbConnection _connection;

        public DataInitializer(IDbConnection connection)
        {
            _connection = connection;
        }

        public void Initialize()
        {
            try
            {
                var sql =
                    @"create table Customers(Id integer primary key, CustomerId text not null, Password text not null)";
                _connection.Execute(sql);
                _connection.Insert(new Customer {CustomerId = "taro", Password = "taro123"});
                _connection.Insert(new Customer {CustomerId = "jiro", Password = "jiro123"});
                _connection.Insert(new Customer {CustomerId = "saburo", Password = "saburo123"});
            }
            catch
            {
                // Do nothing.
            }
        }
    }
}