﻿using System;
using System.Diagnostics;
using Koda.Application.Hosting;
using Koda.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Koda.ConsoleApp
{
    internal static class Program
    {
        private static void Main()
        {
            Initialize();

            using (var scope = ApplicationHost.CreateScope())
            {
                var loginService = scope.ServiceProvider.GetRequiredService<ILoginService>();

                var isLogin = loginService.Login("taro", "taro123");
                Console.WriteLine(isLogin);
                Debug.WriteLine(isLogin);
            }
        }

        private static void Initialize()
        {
            ApplicationHost.Initialize<Startup>();

            using (var scope = ApplicationHost.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<IDataInitializer>().Initialize();
            }
        }
    }
}