﻿using System.Configuration;
using System.Data.Common;
using System.Diagnostics;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class KodaDataConfigurationManagerServiceCollectionExtensions
    {
        public static IServiceCollection AddDefaultDbConnection(this IServiceCollection services)
        {
            var defaultConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            var factory = DbProviderFactories.GetFactory(defaultConnectionString.ProviderName);

            services.AddDbConnection(() =>
            {
                var connection = factory.CreateConnection();
                Debug.Assert(connection != null, nameof(connection) + " != null");
                connection.ConnectionString = defaultConnectionString.ConnectionString;
                return connection;
            });

            return services;
        }
    }
}