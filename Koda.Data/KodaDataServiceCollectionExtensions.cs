﻿using System;
using System.Data;
// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class KodaDataServiceCollectionExtensions
    {
        public static IServiceCollection AddDbConnection(this IServiceCollection services,
            Func<IDbConnection> createConnection)
        {
            services.AddScoped(provider =>
            {
                var connection = createConnection();
                connection.Open();
                return connection;
            });

            return services;
        }
    }
}