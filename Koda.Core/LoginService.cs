﻿namespace Koda.Core
{
    internal sealed class LoginService : ILoginService
    {
        private readonly ICustomerRepository _customers;

        public LoginService(ICustomerRepository customers)
        {
            _customers = customers;
        }

        public bool Login(string customerId, string password)
        {
            var customer = _customers.Find(customerId);
            if (customer == null) return false;
            return customer.Password == password;
        }
    }
}