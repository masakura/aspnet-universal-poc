﻿namespace Koda.Core
{
    public interface ILoginService
    {
        bool Login(string customerId, string password);
    }
}