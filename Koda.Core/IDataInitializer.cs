﻿namespace Koda.Core
{
    public interface IDataInitializer
    {
        /// <summary>
        ///     データベースのスキーマを初期化し、テスト用のデータを挿入します。
        /// </summary>
        void Initialize();
    }
}