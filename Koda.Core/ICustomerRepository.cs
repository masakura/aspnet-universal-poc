﻿namespace Koda.Core
{
    public interface ICustomerRepository
    {
        Customer Find(string customerId);
    }
}