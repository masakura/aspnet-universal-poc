﻿namespace Koda.Core
{
    public sealed class Customer
    {
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string Password { get; set; }
    }
}